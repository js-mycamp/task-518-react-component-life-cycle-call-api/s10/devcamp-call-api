
import "bootstrap/dist/css/bootstrap.min.css"
import FectchAPI from "./component/Fetchapi";
import AxiosLibrary from "./component/AxiosLibrabry";
function App() {
  return (
    <div className="contrainer mt-5 text-center">
      <FectchAPI />
      <hr></hr>
      <AxiosLibrary />
    </div>
  );
}

export default App;
