const { Component } = require("react");

class FectchAPI extends Component {
    fetchAPI = async (url, requestOptions) => {
        // var requestOptions = {
        //     method: 'GET',
        //     redirect: 'follow'
        //   };

        let reponse = await fetch(url, requestOptions)
        let data = await reponse.json()



        return data
    }

    getByIdHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    getAllHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    postHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "title": "foo",
            "body": "bar",
            "userId": 1
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };


        var url = "https://jsonplaceholder.typicode.com/posts";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }

    updateHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "title": "foo update",
            "body": "bar",
            "userId": 1
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    deleteHnadler = () => {
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
          };
        
      
        var url = "https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url,requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        return (
            <div className="row">
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getByIdHandler}>
                        get by ID
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-secondary" onClick={this.getAllHandler}>
                        get all
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-success" onClick={this.postHandler}>
                        Post create
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-warning" onClick={this.updateHandler}>
                        put update
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-danger" onClick={this.deleteHnadler}>
                        delete
                    </button>
                </div>
            </div>
        )
    }
}
export default FectchAPI