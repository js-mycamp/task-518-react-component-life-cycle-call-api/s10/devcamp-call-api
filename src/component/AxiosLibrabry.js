
import axios from "axios";

const { Component } = require("react");

class AxiosLibrary extends Component {
    axiosLibraryCallAPI = async (config) => {

        let reponse = await axios(config)

        return reponse.data
    }

    getByIdHandler = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: {}
        };
        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    getAllHandler = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts',
            headers: {}
        };
        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    postHandler = () => {
        let data = JSON.stringify({
            "userId": 1,
            "title": "sit",
            "body": "co"
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        }; this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }

    updateHandler = () => {
        let data = JSON.stringify({
            "userId": 1,
            "title": "siat",
            "body": "co"
        });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        }; this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })

    }
    deleteHnadler = () => {
        let config = {
            method: 'delete',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: {}
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        return (
            <div className="row">
                <div className="col">
                    <button className="btn btn-primary" onClick={this.getByIdHandler}>
                        get by ID AxiosLibrary
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-secondary" onClick={this.getAllHandler}>
                        get all AxiosLibrary
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-success" onClick={this.postHandler}>
                        Post create AxiosLibrary
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-warning" onClick={this.updateHandler}>
                        put update AxiosLibrary
                    </button>
                </div>
                <div className="col">
                    <button className="btn btn-danger" onClick={this.deleteHnadler}>
                        delete AxiosLibrary
                    </button>
                </div>
            </div>
        )
    }
}
export default AxiosLibrary